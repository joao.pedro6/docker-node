FROM node:lts

WORKDIR /usr/share/app

COPY . .

RUN yarn install

CMD yarn dev