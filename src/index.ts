import express from 'express';
import Knex from 'knex'

const app = express();

const knex = Knex({
    client: 'pg',
    connection: {
        host: 'db',
        port: 5432,
        user: 'user',
        password: 'password',
        database: 'db_name'
    }
});

app.get('/', async (_req, res) => {
    try {
        const db = await knex('')
        res.send('Conectou');
    } catch (error: any) {
        res.send(error.message);
    } finally {
        res.end();
    }
});


const server = app.listen(process.env.PORT, () => console.log('Rodou'));

const stop = () => {
    console.log('vai terminar')
    return server.close((err) => {
        console.log('Http server closed.');
        process.exit(err ? 1 : 0);
    });
};

process.on('SIGINT', stop);
process.on('SIGTERM', stop);